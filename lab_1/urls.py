from django.urls import path
from .views import index, getWheaterData

urlpatterns = [
    path('', index),
    path(r'location/<specific_loc>/', getWheaterData, name='getWheaterData'),
]
