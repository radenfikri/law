from django.shortcuts import render
from django.http import JsonResponse
import requests

def index(request):
    if request.method == 'POST':
        city_name = request.POST['city_name']
        server_url = 'http://152.118.148.95:20331/location/{}'
        server_resp = requests.get(server_url.format(city_name)).json()

        return render(request, 'home.html', server_resp)

    return render(request, 'home.html')

def getWheaterData(request, specific_loc):
    location_url = 'https://www.metaweather.com/api/location/search/?query={}'
    city = specific_loc

    location_resp = requests.get(location_url.format(city)).json()
    
    location = {
        'title' : location_resp[0]['title'],
        'location_type' : location_resp[0]['location_type'],
        'woeid' : location_resp[0]['woeid'],
        'latt_long' : location_resp[0]['latt_long']
    }

    weather_url = 'https://www.metaweather.com/api/location/{}'
    loc_woeid = location['woeid']

    weather_resp = requests.get(weather_url.format(loc_woeid)).json()
    
    city_weather = {
        'the_temp' : weather_resp['consolidated_weather'][0]['the_temp'],
        'air_pressure' : weather_resp['consolidated_weather'][0]['air_pressure'],
        'humidity' : weather_resp['consolidated_weather'][0]['humidity'],
        'wind_speed' : weather_resp['consolidated_weather'][0]['wind_speed'],
        'description' : weather_resp['consolidated_weather'][0]['weather_state_name'],
        'icon' : weather_resp['consolidated_weather'][0]['weather_state_abbr'],
    }
    
    context = {'location' : location, 'city_weather': city_weather}

    return JsonResponse(context)
